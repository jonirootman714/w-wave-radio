# W-Wave-Radio

[Preview](https://w-wave-radio.web.app/)

Это одностраничный сайт который является вымышленной радиоплощадкой «W-Wave-Radio». Предполагается что пользователь будет слушать радио в прямом эфире, ранее записанные подкасты, передачи и т. д., а также видеть информацию о ведущих эфиров и о самом радио. При создании сайта использовал методологию БЭМ а также предпроцессор SASS. В качестве сборщика использовался Webpack.

![alt](docs/Desktop.jpg)

## Запуск проекта

```bash
npm i
```

Сервер для разработки http://localhost:3000

```bash
npm run start
```

Dev-build (no minify):

```bash
npm run dev
```

Production:

```bash
npm run build
```

## APIs

- [Choices](https://github.com/Choices-js/Choices)
- [Accordion](https://github.com/michu2k/Accordion)
- [Swiper](https://swiperjs.com/)
